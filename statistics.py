import urllib
import urllib.request
import json
from collections import Counter
import re
from typing import Dict


class MessageBoardAPIWrapper:
    """
    Wrapper around the messageboard API

    http://localhost:8080/api/
    """

    def __init__(self):
        pass

    def call_api(self, endpoint):
        request = urllib.request.Request('http://0.0.0.0:8080/api/%s' %(endpoint))
        response = urllib.request.urlopen(request)
        return json.loads(response.read())

    def num_messages(self) -> int:
        """
        Returns the total number of messages.
        """
        result = self.call_api('messages')
        # Get the length of the list from the resultant API call
        number_of_messages = len(result)
        return number_of_messages
        raise NotImplementedError

    def most_common_word(self) -> str:
        """
        Returns the most frequently used word in messages.
        """
        result = self.call_api('messages')
        # Get the messages from the resultant API call and store them in a list
        list_of_messages = [message["content"] for message in result]
        """
        1. Join the messages into one big string
        2. Remove some punctuation
        3. Split the one big string into its component strings
        4. Store the result in a list
        """
        list_of_words = ''.join(s for s in list_of_messages).replace('.', ' ').split()
        # Pass the list to an instance of the Counter class
        c = Counter(list_of_words)
        # Use the most_common function from Counter to return the most frequent word from the list and its count
        most_common_words = c.most_common(1)
        most_common_word = most_common_words[0][0]
        return most_common_word
        raise NotImplementedError

    def avg_num_words_per_sentence(self) -> float:
        """
        Returns the average number of words per sentence.
        """
        result = self.call_api('messages')
        # Get the messages from the resultant API call and store them in a list
        list_of_messages = [message["content"] for message in result]
        # Join the messages into one big string
        string_of_all_messages = ''.join(s for s in list_of_messages)
        """
        1. Use regular expressions to split the one big string into its component strings and punctuation
        2. Store the result in a list
        """
        list_of_words_and_punctuation = re.findall(r"[\w']+|[.,?!;]", string_of_all_messages)
        # Create a set of terminals as a "membership test" for a sentence
        terminals = set([".", "?", "!"])
        # Increments the terminal count for each item in a sentence using the defined terminals as a "membership test"
        terminal_count = sum(1 for item in list_of_words_and_punctuation if item in terminals)
        # Calculate the average number of words per sentence using the length of the list and the terminal count
        avg_num_words_per_sentence = (len(list_of_words_and_punctuation) - terminal_count) / float(terminal_count)
        return avg_num_words_per_sentence
        raise NotImplementedError

    def avg_num_msg_thread_topic(self) -> Dict[str, float]:
        """
        Returns the average number of messages per thread, per topic.
        """
        """
        - Each message is associated w/ a thread via the thread id
        - Each thread is associated w/ a topic via the topic id
        - Each topic has an id and there are five topics

        1. Get the titles for each topic and store them in a list
        2. Get the threads associated w/ each topic and store them in a set
        3. Get the threads for each message and store them in a list
        4. Using the threads associated w/ each topic as a membership test, calculate the sum of messages associate w/ each topic
        5. Calculate the average number of messages per thread per topic and store the result in a list
        6. Create and return a dictionary using the list from 1. and the list from 5.
        """
        topic_result = self.call_api('topics')
        # Get the title for each topic from the resultant API call and store them in a list
        list_of_topic_titles = [topic["title"] for topic in topic_result]

        thread_result = self.call_api('threads')
        # Determine the number of threads
        number_of_threads = len(thread_result)

        # Create a set of threads associated with each topic as a "membership test" 
        threads_topic_one = set([thread["id"] for thread in thread_result if thread["topic"] == 1]) # len(threads_topic_one) returns 44

        message_result = self.call_api('messages')
        # Get the threads for each message from the resultant API call and store them in a list
        list_of_threads = [thread["thread"] for thread in message_result]

        # Calculate the sum of messages associated with each topic using the membership test
        sum_messages_topic_one = sum(1 for item in list_of_threads if item in threads_topic_one) # returns 2479, which is the total number of messages associated with topic one

        return list_of_topic_titles

        # Calculate the average number of messages per thread per topic

        raise NotImplementedError

    def _as_dict(self) -> dict:
        """
        Returns the entire messageboard as a nested dictionary.
        """
        topic_result = self.call_api('topics')
        thread_result = self.call_api('threads')
        message_result = self.call_api('messages')

        raise NotImplementedError

    def to_json(self) -> None:
        """
        Dumps the entire messageboard to a JSON file.
        """
        with open("messageboard.json", "w") as f:
            f.write(json.dumps(self._as_dict(), indent=4))


def main():
    """
    Returns information about the messageboard application
    """

    messageboard = MessageBoardAPIWrapper()

    print(f"Total number of messages: {messageboard.num_messages()}")
    print(f"Most common word: {messageboard.most_common_word()}")
    print(
        f"Avg. number of words per sentence.: "
        f"{messageboard.avg_num_words_per_sentence()}"
    )
    print(
        f"Avg. number of messages per thread, per topic.:"
        f"{messageboard.avg_num_msg_thread_topic()}"
    )

    messageboard.to_json()
    print("Message Board written to `messageboard.json`")
    return


if __name__ == "__main__":
    main()
